#!/usr/bin/env python3

from pyswip import Prolog
from pyswip.prolog import PrologError
from gym_sokoban.envs.sokoban_env_fast import SokobanEnvFast
import time
import pandas as pd


def get_swipl_version():
    prolog = Prolog()
    query = list(prolog.query("current_prolog_flag(version, Version)"))
    version_string = str(query[0]['Version'])
    major = int(version_string[:-4])
    minor = int(version_string[-4:-2])
    patch = int(version_string[-2:])
    print("Using swipl version {}.{}.{}".format(major, minor, patch))
    return major, minor, patch

swipl_major_version = get_swipl_version()[0]


def flatten(container):
    for i in container:
        if isinstance(i, (list,tuple)):
            for j in flatten(i):
                yield j
        else:
            yield i


def map_moves(move):
    if move == "up":
        return 3
    elif move == "down":
        return 2
    elif move == "left":
        return 0
    elif move == "right":
        return 1

def generate_rooms(rooms, size, boxes, seed):
    if rooms <= 0:
        return

    dim_room = (size, size)

    env = SokobanEnvFast(dim_room=dim_room,
                         num_boxes=boxes,
                         seed=seed,
                         penalty_for_step=0)
    # The encoding of the board is described in README
    board = env.reset()

    wall = board[:,:,0] # this is a one-hot encoding of walls
    # For readibility first we deal with tops and then with rights
    tops = []
    for i in range(dim_room[0]):
        for j in range(dim_room[1]-1):
            if wall[i,j] == 0 and wall[i,j+1] == 0:
                tops.append("top(x{}y{},x{}y{})".format(i,j,i,j+1))

    rights = []
    for i in range(dim_room[0]-1):
        for j in range(dim_room[1]):
            if wall[i,j] == 0 and wall[i+1,j] == 0:
                rights.append("right(x{}y{},x{}y{})".format(i,j,i+1,j))

    boxes_initial_locations = board[:,:,4]
    boxes_initial = []
    for i in range(dim_room[0]):
        for j in range(dim_room[1]):
            if boxes_initial_locations[i,j] == 1:
                boxes_initial.append("box(x{}y{})".format(i,j))

    boxes_target_locations = board[:,:,2]
    boxes_target = []
    for i in range(dim_room[0]):
        for j in range(dim_room[1]):
            if boxes_target_locations[i,j] == 1:
                boxes_target.append("solution(x{}y{})".format(i,j))

    sokoban_initial_location = board[:,:,5] + board[:,:,6]
    for i in range(dim_room[0]):
        for j in range(dim_room[1]):
            if sokoban_initial_location[i,j] == 1:
                sokoban_string = "sokoban(x{}y{})".format(i,j)
                break

    tops_string = "[" + ','.join(tops) + ']'
    rights_string = "[" + ','.join(rights) + ']'
    boxes_initial_string = "[" + ','.join(boxes_initial) + ']'
    boxes_target_string = "[" + ','.join(boxes_target) + ']'

    if len(boxes_initial) != len(boxes_target):
        # Avoid the bug in the generator by discarding this room
        yield from generate_rooms(rooms, size, boxes, seed + 1)
    else:
        room_string = "[{},{},{},{},{}]".format(tops_string,
                                                rights_string,
                                                boxes_initial_string,
                                                boxes_target_string,
                                                sokoban_string)
        yield env, room_string, seed
        yield from generate_rooms(rooms - 1, size, boxes, seed + 1)


def find_solution(time_limit, env, room_string):
    prolog = Prolog()
    if swipl_major_version < 8:
        print("Warning: using sokoban_swipl7.pl for compatibility with SWI-Prolog version 7")
        prolog.consult("sokoban_swipl7.pl")
    else:
        prolog.consult("sokoban.pl")

    query = "call_with_time_limit({},solve({},Solution))".format(time_limit, room_string)
    if swipl_major_version < 8:
        query = "use_module(library(time))," + query

    print(query)
    try:
        result = list(prolog.query(query))
        if not result:
            print("Solve failed with no solutions found")
            return 0, []

        rewards = []
        for i, r in enumerate(result):
            solution = r['Solution']
            if not solution:
                print("Solve returned an empty move sequence")
                return 0, []

            actions = []
            for step in solution:
                move = str(step).split()[-1]
                move = move[:-1]
                action = map_moves(move)
                actions.append(action)
                observation, reward, done, info = env.step(action)
                rewards.append(reward)
            if rewards[-1] >= 10:
                print("Found solution within time limit")
                return 1, actions
            else:
                print("Solution not good enough:")
                print(solution)
                print("Reward {} is less than 10".format(rewards[-1]))
                return 0, []
    except BaseException as error:
        if isinstance(error, PrologError) and "time_limit_exceeded" in str(error):
            print("Time limit exceeded")
        else:
            print("Error: {}".format(repr(error)))

    return 0, []


if __name__ == "__main__":

    number_of_trials = 100
    start_seed = 0
    time_start = time.time()

    df = pd.DataFrame(columns=['seed', 'actions'])

    rooms = generate_rooms(rooms=number_of_trials, size=8, boxes=2, seed=start_seed)
    results = 0
    for trial, (env, room_string, seed) in enumerate(rooms):
        print("Current trial {} result {}".format(trial, results))
        new_result, actions = find_solution(time_limit=20, env=env, room_string=room_string)
        results += new_result
        df = df.append({'seed' : seed , 'actions' : actions} , ignore_index=True)


    print("Number of solutions: {}".format(results))
    print("Total time: {}".format(time.time() - time_start))
    df.to_csv('results.csv')
    print("Your solutions were written to results.csv")
    print("Remember to run sokoban_verifier.py to verify their correctness")
