# sokoban
Prolog sokoban engine with python frontend.
The prolog code is based on https://github.com/jgsogo/sokoban

# Installation

```sh
virtualenv -p python3 sokoban_venv
source sokoban_venv/bin/activate
pip install -r requirements.txt
```

# Sokoban board generator

We are using

https://gitlab.com/awarelab/gym-sokoban

The encoding of the board is the following

    wall = 0
    empty = 1
    target = 2
    box_target = 3
    box = 4
    player = 5
    player_target = 6
